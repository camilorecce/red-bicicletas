var Bicicleta = require('../../modelos/bicicleta');

exports.bicicleta_list = function(req, res) {
    res.status(200).json({
        bicicletas: Bicicleta.allBicis
    });
}

exports.bicicleta_create = function(req, res) {
    var bicicleta = new Bicicleta(req.body.id, req.body.color, req.body.mdl, [req.body.lat, req.body.long]);
    Bicicleta.add(bicicleta);

    res.status(200).json({
        bicicleta: bicicleta
    });
}

exports.bicicleta_delete = function(req, res) {
    Bicicleta.removeById(req.body.id);

    res.status(204).send();
}

exports.bicicleta_update = function(req, res) {
    var aBici = Bicicleta.findById(req.params.id);

    aBici.id = req.body.id;
    aBici.color = req.body.color;
    aBici.modelo = req.body.mdl;
    aBici.ubicacion = [req.body.lat, req.body.long];

    res.status(200).json({
        bicicleta: aBici
    });
}