const { allBicis } = require('../modelos/bicicleta');
var Bicicleta = require('../modelos/bicicleta');

exports.bicicleta_list = function(req, res) {
    res.render('bicicletas/index', {bicicletas: Bicicleta.allBicis});
}

exports.bicicleta_create_get = function (req, res) {
    res.render('bicicletas/create');
}

exports.bicicleta_create_post = function (req, res) {
    var bicicleta = new Bicicleta(req.body.id, req.body.color, req.body.mdl, [req.body.lat, req.body.long]);
    Bicicleta.add(bicicleta);

res.redirect('/bicicletas');
}

exports.bicicleta_delete_post = function (req, res) {
    Bicicleta.removeById(req.body.id);

    res.redirect('/bicicletas');
}

exports.bicicleta_update_get = function (req, res) {
    var bicicleta = Bicicleta.findById(req.params.id);
    
    res.render('bicicletas/update', {bicicleta});
}

exports.bicicleta_update_post = function (req, res) {
    var bicicleta = Bicicleta.findById(req.params.id);
    bicicleta.id = req.body.id;
    bicicleta.color = req.body.color;
    bicicleta.modelo = req.body.mdl;
    bicicleta.ubicacion = [req.body.lat, req.body.long];

res.redirect('/bicicletas');
}