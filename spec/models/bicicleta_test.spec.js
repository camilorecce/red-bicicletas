var mongoose = require('mongoose');
const bicicleta = require('../../modelos/bicicleta');
var Bicicleta = require('../../modelos/bicicleta');

describe("Testing modelo Bicicletas", function() {
    beforeAll(function(done) {
        var mongoDB = 'mongodb://localhost/testdb';
        mongoose.connect(mongoDB, { useNewUrlParser: true , useUnifiedTopology: true });

        const db = mongoose.connection;
        db.on('error', console.error.bind(console, 'Connection error'));
        db.once('open', function() {
            console.log('Connected to the test database.');
            done();
        });
    });

    afterEach(function(done) {
        Bicicleta.deleteMany({}, function(err, succ) {
            if (err) console.log(err);
            done();
        });
    });


    describe('Bicicleta.createInstance', () => {
        it('crea una instancia de Bicicleta', () => {
            var bici = Bicicleta.createInstance(1, 'rojo', 'urbana', [-34.5, -54.1]);

            expect(bici.code).toBe(1);
            expect(bici.color).toBe('rojo');
            expect(bici.modelo).toBe('urbana');
            expect(bici.ubicacion[0]).toEqual(-34.5);
            expect(bici.ubicacion[1]).toEqual(-54.1);
        })
    });

    describe('Bicicleta.allBicis', () => {
        it('comienza vacia', (done) => {
            Bicicleta.allBicis(function(err, succ) {
                if(err) console.log(err);
                expect(succ.length).toBe(0);
                done();
            })
        });
    });

    describe('Bicicleta.add', () => {
        it('agrega una instancia', (done) => {
            var aBici = new Bicicleta({code: 1, color: 'verde', modelo: 'bmx'});
            Bicicleta.add(aBici, function(err, succ) {
                if(err) console.log(err);
                Bicicleta.allBicis(function(err, succ) {
                    expect(succ.length).toEqual(1);
                    expect(succ[0].code).toEqual(aBici.code);

                    done();
                });
            });
        });
    });

    describe('Bicicleta.findByCode', () => {
        it('devolver bici con code 1', (done) => {
            Bicicleta.allBicis(function(err, bicis) {
                expect(bicis.length).toBe(0);

                var aBici = new Bicicleta({code: 1, color: 'azul', modelo: 'bmx'});
                Bicicleta.add(aBici, function(err, succ) {
                    if (err) console.log(err);

                    var aBici2 = new Bicicleta({code: 2, color: 'verde', modelo: 'urbana'});
                    Bicicleta.add(aBici2, function(err, succ) {
                        if(err) console.log(err);

                        Bicicleta.findByCode(1, function(err, target) {
                            expect(target.code).toBe(aBici.code);
                            expect(target.color).toBe(aBici.color);
                            expect(target.modelo).toBe(aBici.modelo);

                            done();
                        });
                    });
                });
            });
        });
    });



});







/*
beforeEach(() => {
    Bicicleta.allBicis = [];
})

describe("Bicicleta.allBicis", () => {
    it("comienza vacio", () => {
        expect(Bicicleta.allBicis.length).toBe(0);
    });
});

describe("Bicicleta.add", () => {
    it("agregar bicicletas", () => {
        expect(Bicicleta.allBicis.length).toBe(0);

        var a = new Bicicleta(1, 'rojo', 'urbana', [-34.916022, -57.972857]);
        Bicicleta.add(a);

        expect(Bicicleta.allBicis.length).toBe(1);
        expect(Bicicleta.allBicis[0]).toBe(a);
    });
});

describe("Bicicleta.findById", () => {
    it("encontrar por id", () => {
        expect(Bicicleta.allBicis.length).toBe(0);

        var a = new Bicicleta(1, 'verde', 'playera', [-34.95622, -57.992857]);
        var b = new Bicicleta(2, 'azul', 'bmx', [-34.94621, -57.989857]);
        Bicicleta.add(a);
        Bicicleta.add(b);

        var targetBici = Bicicleta.findById(1);
        expect(targetBici).toBe(a);
        expect(targetBici.id).toBe(1);
        expect(targetBici.color).toBe(a.color);
        expect(targetBici.modelo).toBe(a.modelo);

    });
});

describe("Bicicleta.removeById", () => {
    it("eliminar una bicicleta", () => {
        expect(Bicicleta.allBicis.length).toBe(0);

        var a = new Bicicleta(1, 'verde', 'playera', [-34.95622, -57.992857]);
        var b = new Bicicleta(2, 'azul', 'bmx', [-34.94621, -57.989857]);
        Bicicleta.add(a);
        Bicicleta.add(b);

        Bicicleta.removeById(1);
        expect(Bicicleta.allBicis.length).toBe(1);
        expect(Bicicleta.allBicis[0]).toBe(b);
    });
});

describe("Bicicleta.toString", () => {
    it("devolver string correcto", () => {
        var a = new Bicicleta(1, 'verde', 'playera', [-34.95622, -57.992857]);
        Bicicleta.add(a);

        expect(Bicicleta.allBicis[0].toString()).toBe('id: 1 | color: verde');
    })
})
*/