var Bicicleta = require('../../modelos/bicicleta');
var axios = require('axios').default;
var server = require('../../bin/www');

describe('Bicicleta API', () => {
    describe('GET BICICLETAS /', () => {
        it('Status 200', (done) => {
            expect(Bicicleta.allBicis.length).toBe(0);

            var a = new Bicicleta(1, 'verde', 'playera', [-34.95622, -57.992857]);
            Bicicleta.add(a);

            axios.get('http://localhost:3000/api/bicicletas').then(function(response) {
                expect(response.status).toBe(200);
                done();
            });
        });
    });

    describe('POST BICICLETAS /create', () => {
        it('Status 200', (done) => {
            var aBici = {'id':10, 'color':'azul', 'mdl':'mountain', 'lat': -34.96622, 'long': -57.99257};
            axios.post('http://localhost:3000/api/bicicletas/create', aBici).then(function(response) {
                expect(response.status).toBe(200);
                expect(Bicicleta.findById(10).color).toBe('azul');
                done();
            })
        });
    });

    describe('DELETE BICICLETAS /delete', () => {
        it('Status 204', (done) => {
            var a = new Bicicleta(3, 'azul', 'playera', [-34.95522, -57.983857]);
            Bicicleta.add(a);
            
            axios.delete('http://localhost:3000/api/bicicletas/delete', {
                data: {
                    "id": 3
                }
            }).then(function(response) {
                expect(response.status).toBe(204);
                expect(function() {
                    Bicicleta.findById(3)
                }).toThrowError();
                done();
            });
        });
    });

    describe('POST BICICLETAS /update', () => {
        it('Status 200', (done) => {
            var a = new Bicicleta(1, 'verde', 'playera', [-34.95622, -57.992857]);
            var b = new Bicicleta(2, 'azul', 'bmx', [-34.94621, -57.989857]);
            Bicicleta.add(a);
            Bicicleta.add(b);

            axios.post('http://localhost:3000/api/bicicletas/2/update', {
                "id": 5,
                "color": "negro",
                "mdl": "urbana",
                "lat": -34.95622,
                "long": -57.99285
            }).then(function(response) {
                expect(response.status).toBe(200);
                expect(Bicicleta.findById(5).color).toBe('negro');
                done();
            })
        });
    });
});
